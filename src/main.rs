#![no_std]
#![no_main]
#![allow(stable_features)]
#![feature(type_alias_impl_trait)]

extern crate alloc;
use atat::asynch::Client;
use atat::ResponseSlot;
use atat::UrcChannel;
use core::cell::RefCell;
use cortex_m_rt::entry;
use defmt::*;
use embassy_executor::{Executor, Spawner};
use embassy_stm32::gpio::{AnyPin, Input, Level, Output, OutputOpenDrain, Pin, Pull, Speed};
use embassy_stm32::peripherals::PD12;
use embassy_stm32::peripherals::USART3;
use embassy_stm32::time::Hertz;
use embassy_stm32::time::{khz, mhz};
use embassy_stm32::usart::UartTx;
use embassy_stm32::usart::{BufferedUart, BufferedUartRx, BufferedUartTx};
use embassy_stm32::{bind_interrupts, interrupt, peripherals, usart, Config};
use embassy_time::{Duration, Timer};
use static_cell::StaticCell;
use {defmt_rtt as _, panic_probe as _};

use atat::asynch::AtatClient;
use atat::{AtDigester, AtatIngress, DefaultDigester, Ingress, Parser};

use core::mem::MaybeUninit;
use simcom::{
    services::network::NetworkError, DriverError, SimcomConfig, SimcomDevice, SimcomIngress,
    SimcomResponseSlot, SimcomUrcChannel,
};
use stmspeedgovdevice::common;
use {defmt_rtt as _, panic_probe as _};

bind_interrupts!(struct Irqs {
    USART3 => embassy_stm32::usart::BufferedInterruptHandler<peripherals::USART3>;
});
// bind_interrupts!(struct Irqs {
//     USART3 => embassy_stm32::usart::BufferedInterruptHandler<USART3>;
// });
const INGRESS_BUF_SIZE: usize = 1024;
const URC_CAPACITY: usize = 2;
const URC_SUBSCRIBERS: usize = 2;

static EXECUTOR: StaticCell<Executor> = StaticCell::new();

#[global_allocator]
static ALLOCATOR: stmspeedgovdevice::EspHeap = stmspeedgovdevice::EspHeap::empty();

fn init_heap() {
    const HEAP_SIZE: usize = 32 * 1024;
    static mut HEAP: MaybeUninit<[u8; HEAP_SIZE]> = MaybeUninit::uninit();

    unsafe {
        ALLOCATOR.init(HEAP.as_mut_ptr() as *mut u8, HEAP_SIZE);
    }
}

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    init_heap();

    info!("Hello World!");

    let executor = EXECUTOR.init(Executor::new());

    executor.run(|spawner| {
        unwrap!(spawner.spawn(test_uart_device_setup(spawner)));
    })

    // info!("Hello World!");

    // let (ingress, devc, rx14) = test_uart_device_setup(p.PC10, p.PC11, p.USART3);

    // spawner.spawn(ingress_task(ingress, reader)).unwrap();

    // unwrap!(spawner.spawn(ingress_task(ingress, rx14)));
    // unwrap!(spawner.spawn(testsetup(devc)));
}

// #[embassy_executor::task]
// async fn buf_u(mut buf_usart: BufferedUart<'static, embassy_stm32::peripherals::USART3>) {
//     loop {
//         let buf = buf_usart.fill_buf().await.unwrap();
//         info!("Received: {}", buf);

//         // Read bytes have to be explicitly consumed, otherwise fill_buf() will return them again
//         let n = buf.len();
//         buf_usart.consume(n);
//     }
// }

// #[embassy_executor::task]
// pub async fn ingress_task(
//     mut ingress: SimcomIngress<'static, INGRESS_BUF_SIZE>,
//     mut rx: BufferedUartRx<'static, USART3>,
// ) {
//     defmt::info!("reading in ingress_task...");
//     ingress.read_from(&mut rx).await;
// }
#[embassy_executor::task(pool_size = 3)]
async fn blinky(mut led: AnyPin) {
    let mut output = Output::new(led, Level::High, Speed::Low).degrade();
    loop {
        output.set_high();
        Timer::after(Duration::from_millis(1000)).await;
        output.set_low();
        Timer::after(Duration::from_millis(1000)).await;
    }
}

#[embassy_executor::task]
async fn led_blink(p: PD12) {
    let mut led = Output::new(p, Level::High, Speed::Low);

    loop {
        info!("high");
        led.set_high();
        Timer::after(Duration::from_millis(300)).await;

        info!("low");
        led.set_low();
        Timer::after(Duration::from_millis(300)).await;
    }
}

// -> (
//     // atat::asynch::Client<'static, UartTx<'static, hal::peripherals::UART1>, INGRESS_BUF_SIZE>,
//     // SimcomDevice<'static, 'static, atat::asynch::Client<'static, UartTx<'static, UART1>, INGRESS_BUF_SIZE>
//     SimcomIngress<'static, INGRESS_BUF_SIZE>,
//     SimcomDevice<
//         'static,
//         'static,
//         atat::asynch::Client<'static, UartTx<'static, UART1>, INGRESS_BUF_SIZE>,
//         TestConfig,
//     >,
//     UartRx<'static, UART1>,
// )
#[embassy_executor::task]
pub async fn test_uart_device_setup(spawner: Spawner) {
    let mut config = Config::default();
    {
        use embassy_stm32::rcc::*;
        config.rcc.hse = Some(Hse {
            freq: Hertz(8_000_000),
            mode: HseMode::Bypass,
        });
        config.rcc.pll_src = PllSource::HSE;
        config.rcc.pll = Some(Pll {
            prediv: PllPreDiv::DIV4,
            mul: PllMul::MUL180,
            divp: Some(PllPDiv::DIV2), // 8mhz / 4 * 180 / 2 = 180Mhz.
            divq: None,
            divr: None,
        });
        config.rcc.ahb_pre = AHBPrescaler::DIV1;
        config.rcc.apb1_pre = APBPrescaler::DIV4;
        config.rcc.apb2_pre = APBPrescaler::DIV2;
        config.rcc.sys = Sysclk::PLL1_P;
    }
    let p = embassy_stm32::init(config);

    static tx_buf: StaticCell<[u8; 16]> = StaticCell::new();
    static rx_buf: StaticCell<[u8; 16]> = StaticCell::new();

    let (tx_pin, rx_pin, uart) = (p.PC10, p.PC11, p.USART3);
    let mut uart_config = embassy_stm32::usart::Config::default();
    {
        uart_config.baudrate = 115200;
        // uart_config.baudrate = 9600;
        uart_config.parity = embassy_stm32::usart::Parity::ParityNone;
        uart_config.stop_bits = embassy_stm32::usart::StopBits::STOP1;
        uart_config.data_bits = embassy_stm32::usart::DataBits::DataBits8;
    }

    let uart = BufferedUart::new(
        uart,
        Irqs,
        rx_pin,
        tx_pin,
        tx_buf.init([0u8; 16]),
        rx_buf.init([0u8; 16]),
        uart_config,
    );
    let (txPC10, rxPC11) = uart.unwrap().split();

    // static BUFFERS: Buffers<modem::Urc, INGRESS_BUF_SIZE, URC_CAPACITY, URC_SUBSCRIBERS> =
    //     Buffers::<modem::Urc, INGRESS_BUF_SIZE, URC_CAPACITY, URC_SUBSCRIBERS>::new();

    static RES_SLOT: SimcomResponseSlot<INGRESS_BUF_SIZE> = SimcomResponseSlot::new();
    static URC_CHANNEL: SimcomUrcChannel = SimcomUrcChannel::new();
    let ingress = SimcomIngress::new(&RES_SLOT, &URC_CHANNEL);

    // static buf: StaticCell<[u8; INGRESS_BUF_SIZE]> = StaticCell::new();

    let buf = static_cell::make_static!([0; 1024]);
    let config = TestConfig(ResetPin(true));
    let device = SimcomDevice::new(txPC10, &RES_SLOT, &URC_CHANNEL, buf, config);

    // let mut client = Client::new(tx, &RES_SLOT, buf, atat::Config::default());
    //

    // (ingress, device, rx)
    // unwrap!(spawner.spawn(ingress_task(ingress, rxPC11)));
    // unwrap!(spawner.spawn(testsetup(device)));
    unwrap!(spawner.spawn(led_blink(p.PD12)));
}

// #[embassy_executor::task]
// pub async fn testsetup(
//     mut device: SimcomDevice<
//         'static,
//         'static,
//         atat::asynch::Client<'static, BufferedUartTx<'static, USART3>, INGRESS_BUF_SIZE>,
//         TestConfig,
//     >,
// ) {
//     // let m = device.handle;

//     // let tcp = device.data("internet".into()).await.unwrap();
//     match device.data("internet".into()).await {
//         Ok(tcp) => {
//             defmt::info!("{:?}", tcp.local_ip);
//         }
//         Err(e) => match e {
//             DriverError::Atat(atat::Error::Timeout) => {
//                 defmt::error!("[DriverError - Timeout]: {:?}", e);
//             }
//             _ => {}
//         },
//     }

//     // defmt::info!("{:?}", tcp.local_ip);
//     // defmt::info!("Logger is setup");

//     // match device.network().attach(None).await {
//     //     Ok(d) => {
//     //         defmt::info!("network().attach Sent: {:?}", d)
//     //     }
//     //     Err(e) => match e {
//     //         NetworkError::Atat(atat::Error::Timeout) => {
//     //             defmt::error!("[network().attach] - Timeout Error: {:?}", e)
//     //         }
//     //         _ => {}
//     //     },
//     // }
//     // match device.reset().await {
//     //     Ok(d) => {
//     //         defmt::info!("device.reset() Sent: {:?}", d)
//     //     }
//     //     Err(e) => match e {
//     //         DriverError::Atat(atat::Error::Timeout) => {
//     //             defmt::error!("[device.reset()] - Timeout Error: {:?}", e)
//     //         }
//     //         _ => {}
//     //     },
//     // }
//     // match device.setup().await {
//     //     Ok(d) => {
//     //         defmt::info!("device.setup() Sent: {:?}", d)
//     //     }
//     //     Err(e) => match e {
//     //         DriverError::Atat(atat::Error::Timeout) => {
//     //             defmt::error!("[device.setup()] - Timeout Error: {:?}", e)
//     //         }
//     //         DriverError::BaudDetection => {
//     //             defmt::error!("[device.setup()] - BaudDetection Error: {:?}", e)
//     //         }
//     //         _ => {}
//     //     },
//     // }

//     // let mut network = device.network();
//     // network.attach(None).await.unwrap();
// }
use core::convert::Infallible;

use embedded_hal_hal::digital::{ErrorType, OutputPin};
// use embedded_hal::digital::v2::OutputPin;
// use embedded_nal_async::{IpAddr, Ipv4Addr, SocketAddr};

pub struct TestConfig(ResetPin);
pub struct ResetPin(bool);

impl SimcomConfig for TestConfig {
    type ResetPin = ResetPin;

    fn reset_pin(&mut self) -> &mut Self::ResetPin {
        &mut self.0
    }
}

impl OutputPin for ResetPin {
    fn set_low(&mut self) -> Result<(), Self::Error> {
        self.0 = false;
        Ok(())
    }

    fn set_high(&mut self) -> Result<(), Self::Error> {
        self.0 = true;
        Ok(())
    }
}

impl ErrorType for ResetPin {
    type Error = Infallible;
}
